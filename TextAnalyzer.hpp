//------------------------------------------------------------------------------
//
// TextAnalyzer.hpp created by Yyhrs 2019-09-11
//
//------------------------------------------------------------------------------

#ifndef TEXTANALYZER_HPP
#define TEXTANALYZER_HPP

#include <QHash>
#include <QMap>

class TextAnalyzer
{
public:
	enum View
	{
		Deleted,
		Added
	};

	TextAnalyzer() = default;
	~TextAnalyzer() = default;

	void loadDictionary(const QString &language);

	static int		wordcount(const QString &text);
	static QString	simplify(const QString &text, const QString &language);
	static QString	coloredDistance(const QString &left, const QString &right);
	static QString	coloredDistance(const QString &left, const QString &right, View view);

	void	getPhonemic(const QString &text, QStringList &phonemics, const QHash<QString, QString> &prononciation) const;
	QString	phonefy(const QString &text, const QString &language) const;

	double	phonemeDistance(const QString &left, const QString &right, const QString &language) const;
	double	phonemicDistance(const QString &left, const QString &right, const QString &language) const;

private:
	static QString	englishXX(int value);
	static QString	englishXXX(int value);
	static QString	englishNumber(int value);

	static int distanceWeight(const QChar &left, const QChar &right);

	template<typename T, typename P, typename W>
	static QVector<QVector<W>> getLevenshteinDistance(const T &left, const T &right, std::function<W (const P &, const P &)> distanceWeight);

	using Dictionaries = QMap<QString, QHash<QString, QString>>;
	using Factors = QMap<QString, QMap<QPair<QString, QString>, double>>;

	Dictionaries				m_dictionaries;
	Factors						m_factors;

	static const QStringList	s_characterLanguages;
	static const QStringList	s_wordLanguages;
	static const QStringList	s_numbers;
	static const QStringList	s_tenMultiples;
	static const QStringList	s_thousandPowers;
};

#include "TextAnalyzer.tpp"

#endif // STTANALYZER_HPP
