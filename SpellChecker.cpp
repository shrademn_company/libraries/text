//------------------------------------------------------------------------------
//
// SpellChecker.cpp created by Yyhrs 2019-09-11
//
//------------------------------------------------------------------------------

#include <QEvent>
#include <QFile>
#include <QFileInfo>
#include <QStringList>
#include <QTextCodec>
#include <QTextStream>

#include "SpellChecker.hpp"

SpellChecker::SpellChecker(const QString &dictionaryPath, const QString &userDictionary):
	m_affFile{dictionaryPath + ".aff"},
	m_dicFile{dictionaryPath + ".dic"},
	m_hunspell{m_affFile.absoluteFilePath().toLocal8Bit(), m_dicFile.absoluteFilePath().toLocal8Bit()},
	m_userDictionary{userDictionary}
{
	QFile userDictonaryFile{m_userDictionary};

	m_codec = QTextCodec::codecForName(m_hunspell.get_dic_encoding());
	if (userDictonaryFile.open(QIODevice::ReadOnly))
	{
		auto words{QString{userDictonaryFile.readAll()}.split(QChar::CarriageReturn, QString::SkipEmptyParts)};

		for (const auto &word: words)
			m_hunspell.add(m_codec->fromUnicode(word).constData());
		userDictonaryFile.close();
	}
}

bool SpellChecker::hasDictionary()
{
	return m_affFile.exists() && m_dicFile.exists();
}

bool SpellChecker::spell(const QString &word)
{
	return m_hunspell.spell(m_codec->fromUnicode(word).toStdString()) != 0;
}

QStringList SpellChecker::suggest(const QString &word)
{
	auto        suggestions{m_hunspell.suggest(m_codec->fromUnicode(word).toStdString())};
	QStringList result;

	for (const auto &suggestion: suggestions)
		result << m_codec->toUnicode(QByteArray::fromStdString(suggestion));
	return result;
}

void SpellChecker::add(const QString &word)
{
	QFile userDictonaryFile{m_userDictionary};

	if (userDictonaryFile.open(QIODevice::Append))
	{
		QTextStream stream{&userDictonaryFile};

		stream << word << QString{QChar::CarriageReturn};
		userDictonaryFile.close();
	}
	m_hunspell.add(m_codec->fromUnicode(word).constData());
}
