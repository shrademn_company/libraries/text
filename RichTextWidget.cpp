//------------------------------------------------------------------------------
//
// RichTextWidget.cpp created by Thill 02/02/2017
//
//------------------------------------------------------------------------------

#include <QColorDialog>
#include <QMenu>
#include <QTextDocumentFragment>
#include <QTextList>

#include "RichTextWidget.hpp"

RichTextWidget::RichTextWidget(QWidget *parent):
	QWidget{parent}
{
	setupUi(this);
	m_highlighter = new Highlighter{getTextEdit().document()};
	alignmentChanged(textEdit->alignment());
	setColorIcon(actionColor, Qt::black);
	connectActions();
	connectWidgets();
	textEdit->installEventFilter(this);
	toolBar->hide();
}

void RichTextWidget::setOptions(const QList<Option> &options, const QList<QColor> colors)
{
	const QMap<Option, QAction *> basicActions{
		{Bold, actionBold},
		{Italic, actionItalic},
		{Underline, actionUnderline},
		{Strike, actionStrikethrough},
		{Color, actionColor}};

	for (auto option: basicActions.keys())
	{
		if (options.contains(option))
			toolBar->addAction(basicActions[option]);
		else
			basicActions[option]->setDisabled(true);
	}
	if (!colors.isEmpty())
	{
		auto *colorAction{new QAction{"Colors", this}};
		auto *colorMenu{new QMenu{this}};

		for (const auto &color: colors)
		{
			auto *action{new QAction{color.name().toUpper(), this}};

			setColorIcon(action, color);
			connect(action, &QAction::triggered, [this, color]
			{
				setColor(color);
			});
			colorMenu->addAction(action);
		}
		colorAction->setMenu(colorMenu);
		toolBar->addSeparator();
		toolBar->addAction(colorAction);
	}
	if (options.contains(Font))
		toolBar->addWidget(&m_comboFont);
	if (options.contains(FontSize))
		toolBar->addWidget(&m_comboSize);
	if (options.contains(Style))
	{
		toolBar->insertSeparator(actionLeft);
		toolBar->addWidget(&m_comboStyle);
	}
	if (options.contains(Alignment))
	{
		auto *separator{new QAction{this}};

		separator->setSeparator(true);
		toolBar->addActions({separator, actionLeft, actionCenter, actionRight, actionJustify});
	}
	textEdit->addActions(toolBar->actions());
	toolBar->setVisible(options.contains(ToolBar));
}

void RichTextWidget::addCustomAction(QAction *action)
{
	toolBar->addAction(action);
}

QTextEdit &RichTextWidget::getTextEdit()
{
	return *textEdit;
}

Highlighter &RichTextWidget::getHighlighter()
{
	return *m_highlighter;
}

QString RichTextWidget::getHtmlBody()
{
	QRegularExpression regexp("<body.*?>(.*)</body>");

	regexp.setPatternOptions(QRegularExpression::DotMatchesEverythingOption);
	return regexp.match(textEdit->toHtml()).captured(1);
}

void RichTextWidget::setSpellChecker(const QSharedPointer<SpellChecker> &spellChecker)
{
	m_highlighter->setSpellChecker(spellChecker);
}

void RichTextWidget::setSpellChecking(bool on)
{
	m_highlighter->setSpellChecking(on);
}

bool RichTextWidget::event(QEvent *event)
{
	if (event->type() == QEvent::KeyRelease && static_cast<QKeyEvent *>(event)->key() == Qt::Key_Escape)
	{
		textEdit->clearFocus();
		return true;
	}
	else
		return QWidget::event(event);
}

bool RichTextWidget::eventFilter(QObject *watched, QEvent *event)
{
	if (event->type() == QEvent::ContextMenu)
	{
		auto    *contextMenuEvent{static_cast<QContextMenuEvent *>(event)};
		auto    contextMenu{new QMenu{this}};
		auto    *actionEditMenu{new QAction{"Edit", this}};
		auto    cursor{textEdit->textCursor()};
		QString word;

		cursor.setPosition(textEdit->cursorForPosition(contextMenuEvent->pos()).position());
		cursor.select(QTextCursor::WordUnderCursor);
		textEdit->setTextCursor(cursor);
		word = cursor.selectedText();
		contextMenu->addActions(toolBar->actions());
		contextMenu->addSeparator();
		actionEditMenu->setMenu(textEdit->createStandardContextMenu());
		contextMenu->addAction(actionEditMenu);
		if (m_highlighter->spellChecker() && !m_highlighter->spellChecker()->spell(word))
		{
			auto *addWord{new QAction{"Add to dictionary", this}};

			contextMenu->addSeparator();
			connect(addWord, &QAction::triggered, [this, word]
			{
				m_highlighter->spellChecker()->add(word);
				textEdit->insertPlainText(word);
			});
			for (const auto &suggestion: m_highlighter->spellChecker()->suggest(word))
			{
				auto suggestedWord{new QAction{suggestion, this}};

				connect(suggestedWord, &QAction::triggered, [this, suggestion]
				{
					textEdit->insertPlainText(suggestion);
				});
				contextMenu->addAction(suggestedWord);
			}
			if (!cursor.selection().isEmpty())
				contextMenu->addSeparator();
			contextMenu->addAction(addWord);
		}
		contextMenu->exec(contextMenuEvent->globalPos());
		return true;
	}
	return QWidget::eventFilter(watched, event);
}

void RichTextWidget::connectActions()
{
	auto *alignGroup{new QActionGroup{this}};

	alignGroup->addAction(actionLeft);
	alignGroup->addAction(actionCenter);
	alignGroup->addAction(actionRight);
	alignGroup->addAction(actionJustify);
	connect(alignGroup, &QActionGroup::triggered, this, &RichTextWidget::setAlignment);
	connect(actionBold, &QAction::triggered, this, &RichTextWidget::setBold);
	connect(actionItalic, &QAction::triggered, this, &RichTextWidget::setItalic);
	connect(actionUnderline, &QAction::triggered, this, &RichTextWidget::setUnderline);
	connect(actionStrikethrough, &QAction::triggered, this, &RichTextWidget::setStrikethrough);
	connect(actionColor, &QAction::triggered, this, qOverload<>(&RichTextWidget::setColor));
}

void RichTextWidget::connectWidgets()
{
	for (int size: QFontDatabase::standardSizes())
		m_comboSize.addItem(QString::number(size));
	m_comboStyle.addItem("Standard", 0);
	m_comboStyle.addItem("Bullet List (Disc)", QTextListFormat::ListDisc);
	m_comboStyle.addItem("Bullet List (Circle)", QTextListFormat::ListCircle);
	m_comboStyle.addItem("Bullet List (Square)", QTextListFormat::ListSquare);
	m_comboStyle.addItem("Ordered List (Decimal)", QTextListFormat::ListDecimal);
	m_comboStyle.addItem("Ordered List (Alpha lower)", QTextListFormat::ListLowerAlpha);
	m_comboStyle.addItem("Ordered List (Alpha upper)", QTextListFormat::ListUpperAlpha);
	m_comboStyle.addItem("Ordered List (Roman lower)", QTextListFormat::ListLowerRoman);
	m_comboStyle.addItem("Ordered List (Roman upper)", QTextListFormat::ListUpperRoman);
	connect(&m_comboFont, qOverload<const QString &>(&QFontComboBox::activated), this, &RichTextWidget::setFamily);
	connect(&m_comboStyle, qOverload<int>(&QComboBox::activated), this, &RichTextWidget::setStyle);
	connect(&m_comboSize, qOverload<const QString &>(&QComboBox::activated), this, &RichTextWidget::setSize);
	connect(textEdit, &QTextEdit::currentCharFormatChanged, [this](const QTextCharFormat &format)
	{
		fontChanged(format.font());
		setColorIcon(actionColor, format.foreground().color());
	});
	connect(textEdit, &QTextEdit::cursorPositionChanged, [this]
	{
		alignmentChanged(textEdit->alignment());
	});
}

void RichTextWidget::setBold(bool checked)
{
	QTextCharFormat format;

	format.setFontWeight(checked ? QFont::Bold : QFont::Normal);
	mergeFormatOnWordOrSelection(format);
}

void RichTextWidget::setUnderline(bool checked)
{
	QTextCharFormat format;

	format.setFontUnderline(checked);
	mergeFormatOnWordOrSelection(format);
}

void RichTextWidget::setItalic(bool checked)
{
	QTextCharFormat format;

	format.setFontItalic(checked);
	mergeFormatOnWordOrSelection(format);
}

void RichTextWidget::setStrikethrough(bool checked)
{
	QTextCharFormat format;

	format.setFontStrikeOut(checked);
	mergeFormatOnWordOrSelection(format);
}

void RichTextWidget::setFamily(const QString &font)
{
	QTextCharFormat format;

	format.setFontFamily(font);
	mergeFormatOnWordOrSelection(format);
}

void RichTextWidget::setSize(const QString &point)
{
	qreal pointSize{point.toFloat()};

	if (point.toFloat() > 0)
	{
		QTextCharFormat format;

		format.setFontPointSize(pointSize);
		mergeFormatOnWordOrSelection(format);
	}
}

void RichTextWidget::setStyle(int styleIndex)
{
	QTextCursor cursor{textEdit->textCursor()};

	if (styleIndex != 0)
	{
		QTextListFormat::Style style(static_cast<QTextListFormat::Style>(m_comboStyle.itemData(styleIndex).toInt()));

		cursor.beginEditBlock();

		QTextBlockFormat blockFormat(cursor.blockFormat());
		QTextListFormat  listFormat;

		if (cursor.currentList())
			listFormat = cursor.currentList()->format();
		else
		{
			listFormat.setIndent(blockFormat.indent() + 1);
			blockFormat.setIndent(0);
			cursor.setBlockFormat(blockFormat);
		}
		listFormat.setStyle(style);
		cursor.createList(listFormat);
		cursor.endEditBlock();
	}
	else
	{
		QTextBlockFormat blockFormat;

		blockFormat.setObjectIndex(-1);
		cursor.mergeBlockFormat(blockFormat);
	}
}

void RichTextWidget::setColor(const QColor &color)
{
	QTextCharFormat format;

	format.setForeground(color);
	mergeFormatOnWordOrSelection(format);
}

void RichTextWidget::setColor()
{
	auto   cursor{textEdit->textCursor()};
	QColor color{QColorDialog::getColor(textEdit->textColor(), this)};

	if (color.isValid())
	{
		textEdit->setTextCursor(cursor);
		setColor(color);
		setColorIcon(actionColor, color);
	}
}

void RichTextWidget::setAlignment(QAction *action)
{
	if (action == actionLeft)
		textEdit->setAlignment(Qt::AlignLeft | Qt::AlignAbsolute);
	else if (action == actionCenter)
		textEdit->setAlignment(Qt::AlignHCenter);
	else if (action == actionRight)
		textEdit->setAlignment(Qt::AlignRight | Qt::AlignAbsolute);
	else if (action == actionJustify)
		textEdit->setAlignment(Qt::AlignJustify);
}

void RichTextWidget::mergeFormatOnWordOrSelection(const QTextCharFormat &format)
{
	QTextCursor cursor(textEdit->textCursor());

	if (!cursor.hasSelection())
		cursor.select(QTextCursor::WordUnderCursor);
	cursor.mergeCharFormat(format);
	textEdit->mergeCurrentCharFormat(format);
	textEdit->setFocus(Qt::TabFocusReason);
}

void RichTextWidget::fontChanged(const QFont &font)
{
	m_comboFont.setCurrentIndex(m_comboFont.findText(QFontInfo(font).family()));
	m_comboSize.setCurrentIndex(m_comboSize.findText(QString::number(QFontInfo(font).pointSize())));
	actionBold->setChecked(font.bold());
	actionItalic->setChecked(font.italic());
	actionUnderline->setChecked(font.underline());
	actionStrikethrough->setChecked(font.strikeOut());
}

void RichTextWidget::setColorIcon(QAction *action, const QColor &color)
{
	QPixmap pix(24, 24);

	pix.fill(color);
	action->setIcon(pix);
}

void RichTextWidget::alignmentChanged(Qt::Alignment alignment)
{
	if (alignment & Qt::AlignLeft)
		actionLeft->setChecked(true);
	else if (alignment & Qt::AlignHCenter)
		actionCenter->setChecked(true);
	else if (alignment & Qt::AlignRight)
		actionRight->setChecked(true);
	else if (alignment & Qt::AlignJustify)
		actionJustify->setChecked(true);
}
