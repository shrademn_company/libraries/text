//------------------------------------------------------------------------------
//
// Highlighter.cpp created by Yyhrs 2020-02-05
//
//------------------------------------------------------------------------------

#include <QDebug>
#include <QTextBoundaryFinder>

#include "Highlighter.hpp"
#include "SpellChecker.hpp"

Highlighter::Highlighter(QTextDocument *parent):
	QSyntaxHighlighter{parent}
{
}

void Highlighter::addGlossary(int glossary, const QTextCharFormat &format)
{
	m_glossaries[glossary].second = format;
}

void Highlighter::addEntry(int glossary, const QString &entry)
{
	m_glossaries[glossary].first << entry;
}

QSharedPointer<SpellChecker> Highlighter::spellChecker()
{
	return m_spellChecker;
}

void Highlighter::setSpellChecker(const QSharedPointer<SpellChecker> &spellChecker)
{
	if (spellChecker->hasDictionary())
		m_spellChecker = spellChecker;
	else
		m_spellChecker.clear();
}

void Highlighter::setSpellChecking(bool on)
{
	m_spellChecking = on;
	rehighlight();
}

void Highlighter::highlightBlock(const QString &text)
{
	QTextCharFormat     highlightFormat;
	QTextBoundaryFinder wordFinder{QTextBoundaryFinder::Word, text};

	highlightFormat.setUnderlineColor(Qt::darkBlue);
	highlightFormat.setUnderlineStyle(QTextCharFormat::SpellCheckUnderline);
	while (wordFinder.position() < text.size())
	{
		int wordStart{wordFinder.position()};
		int wordLength{wordFinder.toNextBoundary() - wordStart};

		if (wordLength != 1)
		{
			auto word{text.mid(wordStart, wordLength)};

			if (m_spellChecking && wordLength != 1 && m_spellChecker && !m_spellChecker->spell(word))
				setFormat(wordStart, wordLength, highlightFormat);
			for (const auto &glossary: m_glossaries)
				for (const auto &entry: glossary.first)
					if (word.compare(entry, Qt::CaseInsensitive) == 0)
						setFormat(wordStart, wordLength, glossary.second);
		}
	}
}
