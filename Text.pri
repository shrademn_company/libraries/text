INCLUDEPATH += $$PWD $$PWD/hunspell
DEPENDPATH += $$PWD $$PWD/hunspell

SOURCES += \
	$$PWD/Highlighter.cpp \
	$$PWD/RichTextWidget.cpp \
	$$PWD/SpellChecker.cpp \
	$$PWD/TextAnalyzer.cpp

HEADERS += \
	$$PWD/Highlighter.hpp \
	$$PWD/RichTextWidget.hpp \
	$$PWD/SpellChecker.hpp \
	$$PWD/TextAnalyzer.hpp \
	$$PWD/TextAnalyzer.tpp \
	$$PWD/hunspell/affentry.hxx \
	$$PWD/hunspell/affixmgr.hxx \
	$$PWD/hunspell/atypes.hxx \
	$$PWD/hunspell/baseaffix.hxx \
	$$PWD/hunspell/csutil.hxx \
	$$PWD/hunspell/filemgr.hxx \
	$$PWD/hunspell/hashmgr.hxx \
	$$PWD/hunspell/htypes.hxx \
	$$PWD/hunspell/hunspell.h \
	$$PWD/hunspell/hunspell.hxx \
	$$PWD/hunspell/hunvisapi.h \
	$$PWD/hunspell/hunzip.hxx \
	$$PWD/hunspell/langnum.hxx \
	$$PWD/hunspell/phonet.hxx \
	$$PWD/hunspell/replist.hxx \
	$$PWD/hunspell/suggestmgr.hxx \
	$$PWD/hunspell/utf_info.hxx \
	$$PWD/hunspell/w_char.hxx

FORMS += \
	$$PWD/RichTextWidget.ui

DISTFILES += \
	 $$PWD/dictionaries/de-de.ipa \
	 $$PWD/dictionaries/en-gb.factors \
	 $$PWD/dictionaries/en-gb.ipa \
	 $$PWD/dictionaries/en-us.factors \
	 $$PWD/dictionaries/en-us.ipa \
	 $$PWD/dictionaries/es-es.ipa \
	 $$PWD/dictionaries/es-mx.ipa \
	 $$PWD/dictionaries/fr-fr.ipa \
	 $$PWD/dictionaries/ja-jp.ipa \
	 $$PWD/dictionaries/zh-cn.ipa \
	 $$PWD/dictionaries/zh-tw.ipa

dictionaries.path = $$DESTDIR/dictionaries
dictionaries.files = $$PWD/dictionaries/*

INSTALLS += dictionaries

unix: LIBS += -L$$PWD/binaries/g++64bit/ -lhunspell
else:win32: LIBS += -L$$PWD/binaries/msvc64bit/ -llibhunspell

