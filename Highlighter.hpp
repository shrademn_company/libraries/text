//------------------------------------------------------------------------------
//
// Highlighter.hpp created by Yyhrs 2020-02-05
//
//------------------------------------------------------------------------------

#ifndef HIGHLIGHTER_HPP
#define HIGHLIGHTER_HPP

#include <QSyntaxHighlighter>

#include "SpellChecker.hpp"

class Highlighter: public QSyntaxHighlighter
{
	Q_OBJECT

public:
	Highlighter(QTextDocument *parent = nullptr);

	void addGlossary(int glossary, const QTextCharFormat &format);
	void addEntry(int glossary, const QString &entry);

	QSharedPointer<SpellChecker> spellChecker();
	void                         setSpellChecker(const QSharedPointer<SpellChecker> &spellChecker);
	void                         setSpellChecking(bool on);

protected:
	void highlightBlock(const QString &text) override;

private:
	QMap<int, QPair<QSet<QString>, QTextCharFormat>> m_glossaries;
	QSharedPointer<SpellChecker>                     m_spellChecker{nullptr};
	bool                                             m_spellChecking{false};
};

#endif // HIGHLIGHTER_HPP
