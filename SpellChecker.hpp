//------------------------------------------------------------------------------
//
// SpellChecker.hpp created by Yyhrs 2019-09-11
//
//------------------------------------------------------------------------------

#ifndef SPELLCHECKER_H
#define SPELLCHECKER_H

#include <QFileInfo>
#include <QString>

#include <hunspell.hxx>

class SpellChecker
{
public:
	SpellChecker(const QString &dictionaryPath, const QString &userDictionary);
	~SpellChecker() = default;

	bool        hasDictionary();
	bool        spell(const QString &word);
	QStringList suggest(const QString &word);
	void        add(const QString &word);

private:
	QFileInfo  m_affFile;
	QFileInfo  m_dicFile;
	Hunspell   m_hunspell;
	QString    m_userDictionary;
	QTextCodec *m_codec{nullptr};
};

#endif // SPELLCHECKER_H
