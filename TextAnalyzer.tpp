//------------------------------------------------------------------------------
//
// TextAnalyzer.hpp created by Yyhrs 2019-09-11
//
//------------------------------------------------------------------------------

#include "TextAnalyzer.hpp"

template<typename T, typename P, typename W>
QVector<QVector<W>> TextAnalyzer::getLevenshteinDistance(const T &left, const T &right, std::function<W (const P &, const P &)> distanceWeight)
{
	const auto			leftLength{left.length()};
	const auto			rightLength{right.length()};
	QVector<QVector<W>>	distance{leftLength + 1, QVector<W>(rightLength + 1)};

	distance[0][0] = 0;
	for (int i = 1; i <= leftLength; ++i)
		distance[i][0] = i;
	for (int i = 1; i <= rightLength; ++i)
		distance[0][i] = i;
	for (int i = 1; i <= leftLength; ++i)
		for (int j = 1; j <= rightLength; ++j)
			distance[i][j] = std::min({distance[i - 1][j] + 1, distance[i][j - 1] + 1, distance[i - 1][j - 1] + distanceWeight(left[i - 1], right[j - 1])});
	return distance;
}
