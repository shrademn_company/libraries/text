//------------------------------------------------------------------------------
//
// STTAnalyzer.cpp created by Yyhrs 2019-09-11
//
//------------------------------------------------------------------------------

#include <QFile>
#include <QtMath>
#include <QTextBoundaryFinder>
#include <QVector>

#include "TextAnalyzer.hpp"

const QStringList TextAnalyzer::s_characterLanguages{"ja-JP", "zh-CN", "zh-TW"};
const QStringList TextAnalyzer::s_wordLanguages{"de-DE", "en-GB", "en-US", "es-ES", "es-MX", "fr-FR"};
const QStringList TextAnalyzer::s_numbers{"zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen"};
const QStringList TextAnalyzer::s_tenMultiples{"", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety"};
const QStringList TextAnalyzer::s_thousandPowers{"", "thousand", "million", "billion", "trillion", "quadrillion", "quintillion", "sextillion", "septillion", "octillion", "nonillion", "decillion", "undecillion", "duodecillion", "tredecillion", "quattuordecillion", "sexdecillion", "septendecillion", "octodecillion", "novemdecillion", "vigintillion"};

void TextAnalyzer::loadDictionary(const QString &language)
{
	if (!m_dictionaries.contains(language) && (s_wordLanguages.contains(language) || s_characterLanguages.contains(language)))
	{
		QFile file{"dictionaries/" + language.toLower() + ".ipa"};

		if (file.open(QFile::ReadOnly))
		{
			QString line{file.readLine()};

			while (!line.isEmpty())
			{
				m_dictionaries[language][line.section(QChar::Tabulation, 0, 0).toLower()] = line.section(QChar::Tabulation, 1).remove("\r\n");
				line = file.readLine();
			}
		}
	}
	if (!m_factors.contains(language) && (s_wordLanguages.contains(language) || s_characterLanguages.contains(language)))
	{
		QFile file{"dictionaries/" + language.toLower() + ".factors"};

		if (file.open(QFile::ReadOnly))
		{
			QString line{file.readLine()};

			while (!line.isEmpty())
			{
				auto split{line.split(QChar::Tabulation)};

				m_factors[language][{split.at(0), split.at(1)}] = split.at(2).toDouble();
				line = file.readLine();
			}
		}
	}
}

int TextAnalyzer::wordcount(const QString &text)
{
	QTextBoundaryFinder wordFinder{QTextBoundaryFinder::Word, text};
	int                 result{0};

	while (wordFinder.position() < text.size())
	{
		wordFinder.toNextBoundary();
		if (wordFinder.boundaryReasons().testFlag(QTextBoundaryFinder::EndOfItem))
			++result;
	}
	return result;
}

QString TextAnalyzer::simplify(const QString &text, const QString &language)
{
	QString simpleText;

	for (const auto &character: text.simplified().toLower())
		if (character.isSpace() || character.isLetterOrNumber() || character == '\'')
			simpleText.push_back(character);
		else
			simpleText.push_back(QChar::Space);
	if (language.contains("en-"))
	{
		auto words{simpleText.split(QChar::Space)};

		for (auto &word: words)
		{
			bool ok;
			int  number{word.toInt(&ok)};

			if (ok)
				word = englishNumber(number);
		}
		return words.join(QChar::Space);
	}
	return simpleText;
}

QString TextAnalyzer::coloredDistance(const QString &left, const QString &right)
{
	static const QString span{"<%1 style='color:%2;font-size:18px'>%3</%1>"};
	QStringList          result;
	auto                 distance{getLevenshteinDistance<QString, QChar, int>(left, right, &TextAnalyzer::distanceWeight)};
	int                  i{left.length()};
	int                  j{right.length()};
	int                  currentMin{static_cast<int>(distance[i][j])};

	while (i && j)
	{
		int northWest{static_cast<int>(distance[i - 1][j - 1])};
		int north{static_cast<int>(distance[i][j - 1])};
		int west{static_cast<int>(distance[i - 1][j])};
		int min{std::min({north, northWest, west})};

		if (min == currentMin)
		{
			--i;
			result.push_front(right[--j]);
		}
		else if (min == northWest)
		{
			result.push_front(span.arg("sub").arg("#da4453").arg(right[--j]));
			result.push_front(span.arg("sup").arg("#3daee9").arg(left[--i]));
		}
		else if (min == north)
			result.push_front(span.arg("sub").arg("#da4453").arg(right[--j]));
		else if (min == west)
			result.push_front(span.arg("sup").arg("#3daee9").arg(left[--i]));
		currentMin = min;
	}
	while (i)
		result.push_front(span.arg("sup").arg("#3daee9").arg(left[--i]));
	while (j)
		result.push_front(span.arg("sub").arg("#da4453").arg(right[--j]));
	return result.join("").prepend("<font face='source code pro' style='font-size:16px' color='grey'>").append("</font>");
}

QString TextAnalyzer::coloredDistance(const QString &left, const QString &right, View view)
{
	QString     span{"<span style='color:%2'>%3</span>"};
	QStringList result;
	auto        distance{getLevenshteinDistance<QString, QChar, int>(left, right, &TextAnalyzer::distanceWeight)};
	int         i{left.length()};
	int         j{right.length()};
	int         currentMin{static_cast<int>(distance[i][j])};

	span = span.arg(view == Deleted ? "#da4453" : "#3daee9");
	while (i && j)
	{
		int northWest{static_cast<int>(distance[i - 1][j - 1])};
		int north{static_cast<int>(distance[i][j - 1])};
		int west{static_cast<int>(distance[i - 1][j])};
		int min{std::min({north, northWest, west})};

		if (min == currentMin)
		{
			--i;
			result.push_front(QString{right[--j]}.toHtmlEscaped());
		}
		else if (min == northWest)
		{
			--i;
			result.push_front(span.arg(QString{right[--j]}.toHtmlEscaped()));
		}
		else if (min == north)
			result.push_front(span.arg(QString{right[--j]}.toHtmlEscaped()));
		else if (min == west)
			--i;
		currentMin = min;
	}
	while (j)
		result.push_front(span.arg(QString{right[--j]}.toHtmlEscaped()));
	return result.join("").prepend("<font face='source code pro' style='font-size:12px' color='grey'>").append("</font>");
}

void TextAnalyzer::getPhonemic(const QString &text, QStringList &phonemics, const QHash<QString, QString> &prononciation) const
{
	QString processedText{text};

	while (!processedText.isEmpty())
	{
		if (prononciation.contains(processedText))
			return getPhonemic(QString{text}.remove(0, processedText.length()), phonemics << prononciation[processedText], prononciation);
		processedText.remove(processedText.length() - 1, 1);
	}
	if (!text.isEmpty())
		getPhonemic(QString{text}.remove(0, 1), phonemics << QString{text.front()}, prononciation);
}

QString TextAnalyzer::phonefy(const QString &text, const QString &language) const
{
	QStringList postProcessedText;

	if (s_characterLanguages.contains(language) && m_dictionaries.contains(language))
		getPhonemic(text, postProcessedText, m_dictionaries[language]);
	else
	{
		postProcessedText = text.split(QRegExp{" "}, QString::SkipEmptyParts);
		if (m_dictionaries.contains(language))
			for (QString &word: postProcessedText)
			{
				if (m_dictionaries[language].contains(word))
					word = m_dictionaries[language][word];
				else
				{
					bool ok{true};
					int  number{word.toInt(&ok)};

					if (language.contains("en-") && ok)
						word = phonefy(englishNumber(number), language);
					else
					{
						QStringList wordPhonemic;

						getPhonemic(word, wordPhonemic, m_dictionaries[language]);
						word = wordPhonemic.join("");
					}
				}
			}
	}
	return postProcessedText.join("");
}

double TextAnalyzer::phonemeDistance(const QString &left, const QString &right, const QString &language) const
{
	return m_factors.value(language).value({left, right}, left != right);
}

double TextAnalyzer::phonemicDistance(const QString &left, const QString &right, const QString &language) const
{
	int    chunk{20};
	double result{1};
	int    count{0};
	int    length{qMax(left.length(), right.length())};
	auto   distanceWeight([this, language](const QChar &leftChar, const QChar &rightChar)
	{
		return phonemeDistance(leftChar, rightChar, language);
	});

	if (left.isEmpty() || right.isEmpty())
		return 0;
	else if (qMin(left.length(), right.length()) < chunk)
		return 1. - getLevenshteinDistance<QString, QChar, double>(left, right, distanceWeight)[left.length()][right.length()] / length;
	while (count < length)
	{
		QString partLeft{left.mid(count, chunk)};
		QString partRight{right.mid(count, chunk)};
		double  distance;

		if (length < chunk || (partLeft.length() == chunk && partRight.length() == chunk))
		{
			distance = 1. - getLevenshteinDistance<QString, QChar, double>(partLeft, partRight, distanceWeight)[partLeft.length()][partRight.length()] / qMax(partLeft.length(), partRight.length());
			result = qMin(result, distance);
		}
		count += chunk;
	}
	count = chunk;
	while (count < length)
	{
		QString partLeft{left.mid(left.length() - count, chunk)};
		QString partRight{right.mid(right.length() - count, chunk)};
		double  distance;

		if (length < chunk || (partLeft.length() == chunk && partRight.length() == chunk))
		{
			distance = 1. - getLevenshteinDistance<QString, QChar, double>(partLeft, partRight, distanceWeight)[partLeft.length()][partRight.length()] / qMax(partLeft.length(), partRight.length());
			result = qMin(result, distance);
		}
		count += chunk;
	}
	return result;
}

QString TextAnalyzer::englishXX(int value)
{
	if (value < 20)
		return s_numbers[value];
	QString tenMultiple{s_tenMultiples[value / 10]};

	if (value % 10)
		return tenMultiple + " " + s_numbers[value % 10];
	return tenMultiple;
}

QString TextAnalyzer::englishXXX(int value)
{
	QString word;
	int     remainder{value / 100};
	int     modulo{value % 100};

	if (remainder > 0)
		word = s_numbers[remainder] + " hundred ";
	if (modulo > 0)
		word += englishXX(modulo);
	return word;
}

QString TextAnalyzer::englishNumber(int value)
{
	int thousandIndex{0};

	while (++thousandIndex < s_thousandPowers.length())
	{
		int didx{thousandIndex - 1};
		int dval{static_cast<int>(qPow(1000, thousandIndex))};

		if (dval > value)
		{
			int     thousandPower{static_cast<int>(qPow(1000, didx))};
			int     remainder{value / thousandPower};
			int     modulo{value % thousandPower};
			QString result{englishXXX(remainder) + " " + s_thousandPowers[didx]};

			if (modulo)
				result = result + " " + englishNumber(modulo);
			return result;
		}
	}
	return {};
}

int TextAnalyzer::distanceWeight(const QChar &left, const QChar &right)
{
	return left != right;
}
