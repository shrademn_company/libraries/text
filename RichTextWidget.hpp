//------------------------------------------------------------------------------
//
// RichTextWidget.hpp created by Thill 02/02/2017
//
//------------------------------------------------------------------------------

#ifndef RICHTEXTWIDGET_HPP
#define RICHTEXTWIDGET_HPP

#include <QActionGroup>
#include <QComboBox>
#include <QFontComboBox>
#include <QTextCharFormat>
#include <QTextEdit>
#include <QWidget>

#include "Highlighter.hpp"

#include "ui_RichTextWidget.h"

class RichTextWidget: public QWidget, private Ui::RichTextWidget
{
	Q_OBJECT

public:
	enum Option
	{
		Bold,
		Italic,
		Underline,
		Strike,
		Color,
		Font,
		FontSize,
		Style,
		Alignment,
		Menu,
		ToolBar
	};

	explicit RichTextWidget(QWidget*parent = nullptr);

	void setOptions(const QList<Option> &options, const QList<QColor> colors = {});

	void        addCustomAction(QAction *action);
	QTextEdit   &getTextEdit();
	Highlighter &getHighlighter();
	QString     getHtmlBody();

	void setSpellChecker(const QSharedPointer<SpellChecker> &spellChecker);
	void setSpellChecking(bool on);

protected:
	bool event(QEvent *event) override;
	bool eventFilter(QObject *watched, QEvent *event) override;

private:
	void connectActions();
	void connectWidgets();

	void setBold(bool checked);
	void setUnderline(bool checked);
	void setItalic(bool checked);
	void setStrikethrough(bool checked);
	void setFamily(const QString &font);
	void setSize(const QString &point);
	void setStyle(int styleIndex);
	void setColor(const QColor &color);
	void setColor();
	void setAlignment(QAction *action);
	void mergeFormatOnWordOrSelection(const QTextCharFormat &format);
	void fontChanged(const QFont &font);
	void setColorIcon(QAction *action, const QColor &color);
	void alignmentChanged(Qt::Alignment alignment);

	QComboBox     m_comboStyle;
	QFontComboBox m_comboFont;
	QComboBox     m_comboSize;
	Highlighter   *m_highlighter{nullptr};
};

#endif // RICHTEXTWIDGET_HPP
